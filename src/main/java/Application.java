import org.apache.logging.log4j.*;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

        logger.trace("TRACE msg");
        logger.debug("DEBUG msg");
        logger.info("INFO msg");
        logger.warn("WARN msg");
        logger.error("ERROR");
        logger.fatal("FATAL");

    }
}
